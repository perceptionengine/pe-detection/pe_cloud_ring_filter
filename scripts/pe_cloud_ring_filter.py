#!/usr/bin/env python

# Copyright 2021(R) Perception Engine. All rights reserved

from __future__ import print_function

import roslib
roslib.load_manifest('pe_cloud_ring_filter')

import rospy
import numpy as np

from sensor_msgs.msg import PointCloud2
import ros_numpy

class PeCloudRingFilter:
    def __init__(self):
        rospy.init_node("pe_cloud_ring_filter", anonymous=True)

        self.__input_topic = rospy.get_param('~input_topic', 'points_raw')

        rospy.loginfo("Subscribed to: {} [sensor_msgs/PointCloud2]".format(self.__input_topic))
        rospy.Subscriber(self.__input_topic, PointCloud2, self.cloud_callback)

        rospy.loginfo("Publishing to: {} [sensor_msgs/PointCloud2]".format("points_filtered"))
        self.__cloud_filter_pub = rospy.Publisher("points_filtered", PointCloud2, queue_size=1)

        rospy.spin()

    def filter_rings(self, np_cloud, rings):
        filtered = np_cloud[np.in1d(np_cloud['ring'], rings)]
        return filtered

    def publish_np_cloud(self, publisher, np_cloud, header):
        out_cloud_msg = ros_numpy.msgify(PointCloud2, np_cloud)
        out_cloud_msg.header = header

        publisher.publish(out_cloud_msg)

    def cloud_callback(self, cloud_msg):
        raw_cloud = ros_numpy.numpify(cloud_msg)

        filtered_cloud = self.filter_rings(raw_cloud, rings=[1, 2, 3])

        self.publish_np_cloud(self.__cloud_filter_pub, filtered_cloud, cloud_msg.header)


if __name__ == "__main__":
    node = PeCloudRingFilter()
